'''   
   ************** RETO ML                      ***********
   ************** Garazi Alfaro García         ***********
   ************** Patricia Fernández Fernández ***********
'''
import pandas as pd
import numpy as np
from sklearn import preprocessing as sklprep
from sklearn.decomposition import PCA as sklpca

#________________________________________________________
# 

def main():
    fraction_Test = 0.15
    fraction_Valid= 0.2

    # --- Get data -------------------------------------
    DataSet = pd.read_csv('./defectos_madera_entrenamiento.csv',";")

    # Separación del conjunto de datos test, entrenamiento y validación.
    TrainSet, TestSet = split_train_test(DataSet, fraction_Test)
    TrainSet, ValidSet = split_train_test(TrainSet, fraction_Test)

    # Estandarización de los atributos.
    scaler = sklprep.StandardScaler()
    '''
    print(scaler.fit(TrainSet[['X1','X2','X3','X4','X5','X6','X7','X8','X9','X10','X11'
    ,'X12','X13','X14','X15','X16','X17','X18','X19','X20','X21','X22','X23','X24']][:].values))
    print(scaler.mean_)
    print(scaler.transform(TrainSet[['X1','X2','X3','X4','X5','X6','X7','X8','X9','X10','X11'
    ,'X12','X13','X14','X15','X16','X17','X18','X19','X20','X21','X22','X23','X24']][:].values))
   '''
   

    # Estracción de características. (PCA)
    n_components = round(24*0.8)
    feauters = TrainSet[['X1','X2','X3','X4','X5','X6','X7','X8','X9','X10','X11'
    ,'X12','X13','X14','X15','X16','X17','X18','X19','X20','X21','X22','X23','X24']]
    tags = TrainSet[['T1','T2','T3','T4','T5','T6','T7']]
    pca = sklpca(n_components=n_components)
    print(feauters)
    print(pca.fit(feauters))
    print(pca)
    



def split_train_test(data, test_ratio):
    shuffled_indices = np.random.permutation(len(data))
    test_set_size = int(len(data) * test_ratio)
    test_indices = shuffled_indices[:test_set_size]
    train_indices = shuffled_indices[test_set_size:]
    train_set = data.iloc[train_indices]
    test_set  = data.iloc[test_indices]
    return train_set.reset_index(drop=True), test_set.reset_index(drop=True)


main()